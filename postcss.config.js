module.exports = {
    plugins: {
        'postcss-import': {},
        'postcss-preset-env': {
            browsers: 'ie >= 10',
        },
        'cssnano': {},
    },
};
