const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');

module.exports = {
    mode: 'development',
    entry: './app/js/index.js',
    output: {
        filename: 'index.bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [{
            test: /\.(sa|sc|c)ss$/,
            use: [
                MiniCssExtractPlugin.loader,
                'css-loader',
                'postcss-loader',
                'sass-loader',
            ],
        }, {
            test: /\.html$/,
            loaders: [{
                loader: 'html-loader',
                options: {
                    minimize: true,
                    removeAttributeQuotes: false
                }
            }]
        }
            ,
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    'file-loader?name=img/[hash].[ext]&hash=sha512&digest=hex'
                ]
            },
            {
                test: /\.(eot|ttf|woff|woff2)$/,
                loader: 'file-loader?hash=sha512&digest=hex&name=public/fonts/[hash].[ext]'
            }

        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'app') + '/index.html',
            filename: 'index.html'
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'app') + '/profil.html',
            filename: 'profil.html'
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'app') + '/profil-form.html',
            filename: 'profil-form.html'
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'app') + '/under-construction.html',
            filename: 'under-construction.html'
        }),
        new MiniCssExtractPlugin({
            filename: '[name].bundle.css',
            chunkFilename: '[id].bundle.css',
        }),
        new FaviconsWebpackPlugin(path.resolve(__dirname, 'app') + '/icon.png')
    ]
};
